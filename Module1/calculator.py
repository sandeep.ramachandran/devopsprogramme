import operator
def isInputAnInteger(parameterPassed):
    try:
        IntegerConverted = int(parameterPassed)
        return True
    except:
        return False

def ValidateTheInputOperation(operationType):
    strippedOperationType = operationType.strip()
    if (strippedOperationType=="+") or (strippedOperationType=="-") or (strippedOperationType=="x") or (strippedOperationType=="/"):
        return strippedOperationType
    else:
        return ""

#input the numbers for operation
operationToBePerformed = input("Enter the operation to be performed. Available options are +,-,/,x")

while ValidateTheInputOperation(operationToBePerformed)=="":
    print("Enter a Valid operator")
    operationToBePerformed = input("Enter the operation to be performed. Available options are +,-,/,x")

#input the numbers for operation
Integer1 = input("Enter the first integer")
while isInputAnInteger(Integer1)==False:
    print("Enter a Valid Integer")
    Integer1 = input("Enter the first integer")
Integer1 = int(Integer1)
#input the numbers for operation
Integer2 = input("Enter the first integer")
while isInputAnInteger(Integer2)==False:
    print("Enter a Valid Integer")
    Integer2 = input("Enter the second integer")
Integer2 = int(Integer2)

ops = { "+": operator.add, "-": operator.sub, "x": operator.mul, "/": operator.truediv }
print(ops[operationToBePerformed](Integer1,Integer2))