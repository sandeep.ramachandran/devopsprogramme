'''
################### Input arguments ###################
users = input("What is your name")
print(f'my name is {users}')
'''
#print(f'my name is hardcoded')

'''
################### Restrictions on variable names ###################
import keyword
print(keyword.kwlist)
'''

'''
################### Use of Unicodes UTF-8 ###################
message = "I ❤ Python"
print(message)
'''
'''
################### All strings are arrays by default ###################
AllStrArray="Sandeep"
print(AllStrArray[0])
print(AllStrArray[len(AllStrArray)-1])

for i in AllStrArray:
    print(i)

'''

'''
################### mid, left, right of string in no time ###################
original_string ='SandeepRamachandran'
print(original_string[0:5]) # 'Super'
print(original_string[5:9]) # 'cali'
print(original_string[:5]) # 'Super'
print(original_string[7:]) #'fragilistic'
'''

'''
################### Easierst string comparison ###################
print("apple" < "apana") # True, using alphabeticalorder
print("coconut" == "coco" + "nut") # True
'''

'''
################### Altering a string with ease ###################
my_string = 'Sandeep'
print(my_string)
my_string = 'd' + my_string[1]
print(my_string)
'''

'''
################### Append list by insertion ###################
test=[1,2,3,4]
test.insert(2,8)
print(test)
'''

'''
################### Hint using ->, but is just reference and is ignored by interpreter ###################

def multiply(a, b) -> int:
    return a * b
'''

'''
################### return None ###################
def empty_return():
 return
variable = empty_return()
print(variable) 
'''

def greet_user(first, surname):
    if surname == '':
        print('You are missing a surname ' + first + '!')
        return
    full_name = first + ' ' + surname
    print('Hey there, ' + full_name)
    # This would print 'You are missing asurname Jane!' (and nothing else)

greet_user('Jane')